/************************************************************************/
/*									*/
/* File: io_rcc.c							*/
/*									*/
/* IO RCC driver							*/
/*									*/
/*  7. Jun. 02  MAJO  created						*/
/*									*/
/************ C 2005 - The software with that certain something *********/

/************************************************************************/
/*NOTES:								*/
/*- This driver should work on kernels from 2.4 onwards			*/
/************************************************************************/

#include <linux/config.h>
#if defined(CONFIG_MODVERSIONS) && !defined(MODVERSIONS)
  #define MODVERSIONS
#endif

//MJ-SMP:#ifdef CONFIG_SMP
//MJ-SMP:  #define __SMP__
//MJ-SMP:#endif

#if defined(MODVERSIONS)
  #include <linux/modversions.h>
#endif
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <linux/string.h>
#include <linux/vmalloc.h>
#include <linux/mman.h>
#include <linux/wrapper.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <asm/system.h>
#include "io_rcc/io_rcc_driver.h"

#ifdef MODULE
  MODULE_PARM (debug, "i");
  MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging");
  MODULE_DESCRIPTION("PCI IO driver");
  MODULE_AUTHOR("Markus Joos, CERN/EP");
  #ifdef MODULE_LICENSE
    MODULE_LICENSE("Private: Contact markus.joos@cern.ch");
  #endif
#endif


/*********/
/*Globals*/
/*********/
static int debug = 0;
static int iorcc_major = 0; // use dynamic allocation
static u_int board_type;
static char *proc_read_text;
static struct proc_dir_entry *io_rcc_file;
static pci_devices_t pcidev[IO_MAX_PCI];
struct io_proc_data_t io_proc_data;

struct vm_operations_struct io_vm_ops =
{       
  close: io_rcc_vmaClose
};


static struct file_operations fops = 
{
  ioctl:   io_rcc_ioctl,
  open:    io_rcc_open,    
  mmap:    io_rcc_mmap,
  release: io_rcc_release
};

/****************************/
/* Standard driver function */
/****************************/


/***********************************************************/
static int io_rcc_open(struct inode *ino, struct file *filep)
/***********************************************************/
{
  char *buf;
  private_stuff *pptr; 
  int loop;

  //reserve space to store information about the devices managed by this "file"
  buf = (char *)kmalloc(sizeof (private_stuff), GFP_KERNEL);
  if (buf == NULL)
  {
    kdebug(("io_rcc_drv(io_rcc_open): error from kmalloc\n"));
    return(-EFAULT);
  }

  //Initialize the space
  pptr = (private_stuff *) buf;
  for (loop = 0; loop < IO_MAX_PCI; loop++)
    pptr->linked[loop] = 0;

  filep->private_data = buf;
  kdebug(("io_rcc_drv(io_rcc_open): private_data = 0x%08x\n", filep->private_data));
  
  return(0);
}


/**************************************************************/
static int io_rcc_release(struct inode *ino, struct file *filep)
/**************************************************************/
{
  int loop;
  private_stuff *pptr; 

  kdebug(("io_rcc(release): pid = %d\n", current->pid));
  pptr = (private_stuff *) filep->private_data;

  // Release orphaned links to PCI devices
  for(loop = 0; loop < IO_MAX_PCI; loop++)
  {
    if (pptr->linked[loop] == 1)
    {
      kdebug(("io_rcc(release): Orphaned PCI device unlinked (pid=%d: vid=0x%08x did=0x%08x)\n", pcidev[loop].pid, pcidev[loop].vid, pcidev[loop].did));
      pcidev[loop].pid = 0;
      pptr->linked[loop] = 0;
    } 
  }

  return(0);
}


/**************************************************************************************************/
static int io_rcc_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
/**************************************************************************************************/
{
  private_stuff *pptr; 

  pptr = (private_stuff *) file->private_data;

  switch (cmd)
  {        
    case IOPEEK:
    {
      IO_RCC_IO_t params;
      int ret;
      
      ret = copy_from_user(&params, (void *)arg, sizeof(IO_RCC_IO_t));
      if (ret)
      {
	kdebug(("io_rcc(ioctl,IOPEEK): error %d from copy_from_user\n",ret));
	return(-EFAULT);
      }
      
      if (params.size == 4)
      {
        params.data = inl(params.offset);
        kdebug(("io_rcc(ioctl,IOPEEK): 0x%08x (int) read from 0x%08x\n", params.data, params.offset));
      }
      if (params.size == 2)
      {
        params.data = inw(params.offset);      
        kdebug(("io_rcc(ioctl,IOPEEK): 0x%08x (word) read from 0x%08x\n", params.data, params.offset));
      }
      if (params.size == 1)
      {
        params.data = inb(params.offset);
        kdebug(("io_rcc(ioctl,IOPEEK): 0x%08x (byte) read from 0x%08x\n", params.data, params.offset));
      }
        
      if (copy_to_user((void *)arg, &params, sizeof(IO_RCC_IO_t)) != 0)
      {
	kdebug(("io_rcc(ioctl,IOPEEK): error from copy_to_user\n"));
	return(-EFAULT);
      }
      break;
    }   
        
    case IOPOKE:
    {
      IO_RCC_IO_t params;
      int ret;
      
      ret = copy_from_user(&params, (void *)arg, sizeof(IO_RCC_IO_t));
      if (ret)
      {
	kdebug(("io_rcc(ioctl,IOPOKE): error %d from copy_from_user\n",ret));
	return(-EFAULT);
      }
      
      if (params.size == 4) 
      {
        kdebug(("io_rcc(ioctl,IOPOKE): writing 0x%08x (int) to 0x%08x\n", params.data, params.offset));
        outl(params.data, params.offset);
        wmb();    // Recommended by Rubini on p. 238
      }
      if (params.size == 2) 
      {
        kdebug(("io_rcc(ioctl,IOPOKE): writing 0x%08x (word) to 0x%08x\n", params.data, params.offset));
        outw(params.data&0xffff, params.offset);
        wmb();
      }
      if (params.size == 1) 
      {
        kdebug(("io_rcc(ioctl,IOPOKE): writing 0x%08x (byte) to 0x%08x\n", params.data, params.offset));
        outb(params.data&0xff, params.offset);
        wmb();
      }
              
      if (copy_to_user((void *)arg, &params, sizeof(IO_RCC_IO_t)) != 0)
      {
	kdebug(("io_rcc(ioctl,IOPOKE): error from copy_to_user\n"));
	return(-EFAULT);
      }
      break;
    } 
     
    case IOGETID:
    {
      if (copy_to_user((void *)arg, &board_type, sizeof(int)) != 0)
      {
    	kdebug(("io_rcc(ioctl,IOGETID): error from copy_to_user\n"));
      return(-EFAULT);
      }
      break;
    }
    
    case IOPCILINK:
    {
      int ret, dnum;
      u_int loop;
      IO_PCI_FIND_t find;
      struct pci_dev *dev = NULL;
      
      ret = copy_from_user(&find, (void *)arg, sizeof(IO_PCI_FIND_t));
      if (ret)
      {
	kdebug(("io_rcc(ioctl,IOPCILINK): error %d from copy_from_user\n",ret));
	return(-EFAULT);
      }     
      
      //Find a free slot in the pcidev array
      //MJ: for now I am not checking if the same device has already been opened by an other call
      dnum = -1;
//MJ-SMP: to be protected (spinlock)
      for(loop = 0; loop < IO_MAX_PCI; loop++)
      {
        if (pcidev[loop].pid == 0)
        {
          dnum = loop;
          pcidev[dnum].pid = current->pid;  //Reserve this array element
          pptr->linked[dnum] = 1;           //Remember to which "file" this mapping belongs to
          break;
        }
      }
//MJ-SMP: end of protected zone      
      
      if (dnum == -1)
      {
        kdebug(("io_rcc(ioctl,IOPCILINK): Device table is full\n"));
        return(-IO_PCI_TABLEFULL);
      }
          
      for(loop = 0; loop < find.occ; loop++)
      {
        dev = pci_find_device(find.vid, find.did, dev);  //Find N-th device      
        if (dev == NULL)
        {
          kdebug(("io_rcc(ioctl,IOPCILINK): No device found\n"));
          pcidev[dnum].pid = 0;  //array element no longer required
          pptr->linked[dnum] = 0;
          return(-IO_PCI_NOT_FOUND);
        }
      }
  
      kdebug(("io_rcc(ioctl,IOPCILINK):Device found\n"));
      kdebug(("io_rcc(ioctl,IOPCILINK):devfn =0x%08x\n", dev->devfn));
      kdebug(("io_rcc(ioctl,IOPCILINK):irq   =0x%08x\n", dev->irq));

      pcidev[dnum].vid     = find.vid;
      pcidev[dnum].did     = find.did;
      pcidev[dnum].occ     = find.occ;
      pcidev[dnum].dev_ptr = dev;
      
      find.handle = dnum;
    
      ret = copy_to_user((void *)arg, &find, sizeof(IO_PCI_FIND_t));
      if (ret)
      {
    	kdebug(("io_rcc(ioctl,IOPCILINK): error from copy_to_user\n"));
        return(-EFAULT);
      }
      break;
    }
    
    case IOPCIINFO:
    {
      int ret, loop;
      pci_info_t info;   

      ret = copy_from_user(&info, (void *)arg, sizeof(pci_info_t));
      if (ret)
      {
	kdebug(("io_rcc(ioctl,IOPCIINFO): error %d from copy_from_user\n", ret));
	return(-EFAULT);
      }  
      
      kdebug(("io_rcc(ioctl,IOPCIINFO): Info requested for handle %d\n", info.handle));
      
      pcidev[info.handle].dev_ptr;

      for (loop = 0; loop < 6; loop++)
      {
        info.bar[loop].base  = pci_resource_start(pcidev[info.handle].dev_ptr, loop);
        info.bar[loop].size  = pci_resource_len(pcidev[info.handle].dev_ptr, loop);
        //if (info.bar[loop].size == 1)  //Catch unused BARs
          //info.bar[loop].size = 0;
        info.bar[loop].flags = pci_resource_flags(pcidev[info.handle].dev_ptr, loop);
        kdebug(("io_rcc(ioctl,IOPCIINFO): BAR %d:\n", loop));
        kdebug(("io_rcc(ioctl,IOPCIINFO): base  = 0x%08x:\n", info.bar[loop].base));
        kdebug(("io_rcc(ioctl,IOPCIINFO): end   = 0x%08x:\n", info.bar[loop].size));
        kdebug(("io_rcc(ioctl,IOPCIINFO): flags = 0x%08x:\n", info.bar[loop].flags));
      }

      ret = copy_to_user((void *)arg, &info, sizeof(pci_info_t));
      if (ret)
      {
	kdebug(("io_rcc(ioctl,IOPCIINFO): error %d from copy_to_user\n", ret));
	return(-EFAULT);
      }  

      break;
    }
    
    case IOPCIUNLINK:
    { 
      u_int dnum;
      int ret;
 
      ret = copy_from_user(&dnum, (void *)arg, sizeof(int));
      if (ret)
      {
	kdebug(("io_rcc(ioctl,IOPCIUNLINK): error %d from copy_from_user\n",ret));
	return(-EFAULT);
      }
      
      if (pcidev[dnum].pid == 0)
      {
        kdebug(("io_rcc(ioctl,IOPCICONFR): Illegal handle\n"));
	return(-IO_PCI_ILL_HANDLE);
      }
      
      pcidev[dnum].pid = 0;
      pptr->linked[dnum] = 0;

      break;
    }
    
    case IOPCICONFR:
    {
      int ret;
      IO_PCI_CONF_t params;
      u_int idata;
      u_short sdata;
      u_char cdata;
    
      ret = copy_from_user(&params, (void *)arg, sizeof(IO_PCI_CONF_t));
      if (ret)
      {
	kdebug(("io_rcc(ioctl,IOPCICONFR): error %d from copy_from_user\n",ret));
	return(-EFAULT);
      }
      
      if (pcidev[params.handle].pid == 0)
      {
        kdebug(("io_rcc(ioctl,IOPCICONFR): Illegal handle\n"));
	return(-IO_PCI_ILL_HANDLE);
      }
      
      if (params.size == 4)
      {
        ret = pci_read_config_dword(pcidev[params.handle].dev_ptr, params.offs, &idata);
        params.data = idata;
      }
      if (params.size == 2)
      {
        ret = pci_read_config_word(pcidev[params.handle].dev_ptr, params.offs, &sdata);
        params.data = sdata;
      }
      if (params.size == 1)
      {
        ret = pci_read_config_byte(pcidev[params.handle].dev_ptr, params.offs, &cdata);
        params.data = cdata;
      }
        
      if (ret)
      {
        kdebug(("io_rcc(ioctl,IOPCICONFR): ERROR from pci_read_config_xxx\n"));
	return(-IO_PCI_CONFIGRW);
      }
      
      ret = copy_to_user((void *)arg, &params, sizeof(IO_PCI_CONF_t));
      if (ret)
      {
    	kdebug(("io_rcc(ioctl,IOPCICONFR): error from copy_to_user\n"));
        return(-EFAULT);
      }
      
      break;
    }

    case IOPCICONFW:   
    {
      int ret;
      IO_PCI_CONF_t params;
    
      ret = copy_from_user(&params, (void *)arg, sizeof(IO_PCI_CONF_t));
      if (ret)
      {
	kdebug(("io_rcc(ioctl,IOPCICONFW): error %d from copy_from_user\n",ret));
	return(-EFAULT);
      }
      
      if (pcidev[params.handle].pid == 0)
      {
        kdebug(("io_rcc(ioctl,IOPCICONFW): Illegal handle\n"));
	return(-IO_PCI_ILL_HANDLE);
      }
      
      
      if (params.size == 4)
        ret = pci_write_config_dword(pcidev[params.handle].dev_ptr, params.offs, params.data);
      if (params.size == 2)
        ret = pci_write_config_dword(pcidev[params.handle].dev_ptr, params.offs, params.data&0xffff);
      if (params.size == 1)
        ret = pci_write_config_dword(pcidev[params.handle].dev_ptr, params.offs, params.data&0xff);
      
      if (ret)
      {
        kdebug(("io_rcc(ioctl,IOPCICONFW): ERROR from pci_write_config_xxxx\n"));
	return(-IO_PCI_CONFIGRW);
      }
      
      break;
    }

  }
  return(0);
}


/*****************************************************/
static void io_rcc_vmaClose(struct vm_area_struct *vma)
/*****************************************************/
{ 
  kdebug(("io_rcc_vmaClose: mmap released\n"));
}  


/*******************************************************************/
static int io_rcc_mmap(struct file *file, struct vm_area_struct *vma)
/*******************************************************************/
{
  int result;
  u_long size, offset;  

  vma->vm_flags |= VM_RESERVED;

  // we do not want to have this area swapped out, lock it
  vma->vm_flags |= VM_LOCKED;
  

  kdebug(("io_rcc_mmap: vma->vm_end    = 0x%016lx\n", (u_long)vma->vm_end));
  kdebug(("io_rcc_mmap: vma->vm_start  = 0x%016lx\n", (u_long)vma->vm_start));
  kdebug(("io_rcc_mmap: vma->vm_offset = 0x%016lx\n", (u_long)vma->vm_pgoff << PAGE_SHIFT));
  kdebug(("io_rcc_mmap: vma->vm_flags  = 0x%08x\n", (u_int)vma->vm_flags));

  size = vma->vm_end - vma->vm_start;

  offset = vma->vm_pgoff << PAGE_SHIFT;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,4,20)
  result = remap_page_range(vma, vma->vm_start, offset, size, vma->vm_page_prot);
#else
  result = remap_page_range(vma->vm_start, offset, size, vma->vm_page_prot);
#endif
  if (result)
  {
    kdebug(("io_rcc_mmap: function remap_page_range failed \n"));
    return(-IO_PCI_REMAP);
  }
  kdebug(("io_rcc_mmap: vma->vm_start(2) = 0x%016lx\n", (u_long)vma->vm_start));

  vma->vm_ops = &io_vm_ops;
  return(0);
}


/*****************************************************************************************************/
static int io_rcc_write_procmem(struct file *file, const char *buffer, unsigned long count, void *data)
/*****************************************************************************************************/
{
  int res, len;
  u_int card;
  struct io_proc_data_t *fb_data = (struct io_proc_data_t *)data;

  kdebug(("io_rcc_mmap(io_rcc_write_procmem): io_rcc_write_procmem called\n"));

  if(count > 99)
    len = 99;
  else
    len = count;

  if (copy_from_user(fb_data->value, buffer, len))
  {
    kdebug(("io_rcc_mmap(io_rcc_write_procmem): error from copy_from_user\n"));
    return(-EFAULT);
  }

  kdebug(("io_rcc_mmap(io_rcc_write_procmem): len = %d\n", len));
  fb_data->value[len - 1] = '\0';
  kdebug(("io_rcc_mmap(io_rcc_write_procmem): text passed = %s\n", fb_data->value));

  if (!strcmp(fb_data->value, "debug"))
  {
    debug = 1;
    kdebug(("io_rcc_mmap(io_rcc_write_procmem): debugging enabled\n"));
  }

  if (!strcmp(fb_data->value, "nodebug"))
  {
    kdebug(("io_rcc_mmap(io_rcc_write_procmem): debugging disabled\n"));
    debug = 0;
  }

  return len;
}


/****************************************************************************************************/
static int io_rcc_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data)
/****************************************************************************************************/
{
  int loop, nchars = 0;
  static int len = 0;

  kdebug(("io_rcc_read_procmem: Called with buf    = 0x%08x\n", (unsigned int)buf));
  kdebug(("io_rcc_read_procmem: Called with *start = 0x%08x\n", (unsigned int)*start));
  kdebug(("io_rcc_read_procmem: Called with offset = %d\n", (unsigned int)offset));
  kdebug(("io_rcc_read_procmem: Called with count  = %d\n", count));

  if (offset == 0)
  {
    kdebug(("io_rcc_read_procmem: Creating text....\n"));
    len = 0;
    len += sprintf(proc_read_text + len, "IO RCC driver for release %s (based on CVS tag %s)\n", RELEASE_NAME, CVSTAG);
    len += sprintf(proc_read_text + len, "Dumping table of linked devices\n");
    len += sprintf(proc_read_text + len, "Handle | Vendor ID | Device ID | Occurrence | Process ID\n");
    for(loop = 0; loop < IO_MAX_PCI; loop++)
    {
      if (pcidev[loop].pid != 0)
      {
      len += sprintf(proc_read_text + len, "    %2d |", loop);
      len += sprintf(proc_read_text + len, "0x%08x |", pcidev[loop].vid);
      len += sprintf(proc_read_text + len, "0x%08x |", pcidev[loop].did);
      len += sprintf(proc_read_text + len, " 0x%08x |", pcidev[loop].occ);
      len += sprintf(proc_read_text + len, " 0x%08x\n", pcidev[loop].pid);
      }
    }

    len += sprintf(proc_read_text + len, " \n");
    len += sprintf(proc_read_text + len, "The command 'echo <action> > /proc/io_rcc', executed as root,\n");
    len += sprintf(proc_read_text + len, "allows you to interact with the driver. Possible actions are:\n");
    len += sprintf(proc_read_text + len, "debug   -> enable debugging\n");
    len += sprintf(proc_read_text + len, "nodebug -> disable debugging\n");
  }
  kdebug(("io_rcc_read_procmem: number of characters in text buffer = %d\n", len));

  if (count < (len - offset))
    nchars = count;
  else
    nchars = len - offset;
  kdebug(("io_rcc_read_procmem: min nchars         = %d\n", nchars));
  
  if (nchars > 0)
  {
    for (loop = 0; loop < nchars; loop++)
      buf[loop + (offset & (PAGE_SIZE - 1))] = proc_read_text[offset + loop];
    *start = buf + (offset & (PAGE_SIZE - 1));
  }
  else
  {
    nchars = 0;
    *eof = 1;
  }
 
  kdebug(("io_rcc_read_procmem: returning *start   = 0x%08x\n", (unsigned int)*start));
  kdebug(("io_rcc_read_procmem: returning nchars   = %d\n", nchars));
  return(nchars);
}


/*******************/
int init_module(void)
/*******************/
{
  int result, loop;
  u_char reg;

  SET_MODULE_OWNER(&fops);

  // Read the board identification
  outb(BID1, CMOSA);
  reg = inb(CMOSD);
  kdebug(("io_rcc(init_module): BID1 = 0x%02x\n", reg));
  
  if (!(reg&0x80))
  {
    kdebug(("io_rcc(init_module): Unable to determine board type\n"));
    board_type = VP_UNKNOWN;
  }
  else
  {
    outb(BID2, CMOSA);
    reg = inb(CMOSD);
    kdebug(("io_rcc(init_module): BID2 = 0x%02x\n", reg));

    reg &= 0x1f;  // Mask board ID bits
    board_type = VP_UNKNOWN;
         if (reg == 2) board_type = VP_PSE;  // VP-PSE
    else if (reg == 3) board_type = VP_PSE;  // VP-PSE
    else if (reg == 4) board_type = VP_PSE;  // VP-PSE
    else if (reg == 5) board_type = VP_PMC;  // VP-PMC
    else if (reg == 6) board_type = VP_100;  // VP-100
    else if (reg == 7) board_type = VP_CP1;  // VP-CP1
    else  board_type = 0;
    if (board_type == VP_PSE)
      {kdebug(("io_rcc(init_module): Board type = VP-PSE\n"));}
    if (board_type == VP_PMC)
      {kdebug(("io_rcc(init_module): Board type = VP-PMC\n"));}
    if (board_type == VP_100)
      {kdebug(("io_rcc(init_module): Board type = VP-100\n"));}
    if (board_type == VP_CP1)
      {kdebug(("io_rcc(init_module): Board type = VP-CP1\n"));}
   
    if (!board_type)
      {kdebug(("io_rcc(init_module): Unable to determine board type(2)\n"));}
  }
  

  result = register_chrdev(iorcc_major, "io_rcc", &fops); 
  if (result < 1)
  {
    kdebug(("io_rcc(init_module): register IO RCC driver failed.\n"));
    return(-EIO);
  }
  iorcc_major = result;

  proc_read_text = (char *)kmalloc(MAX_PROC_TEXT_SIZE, GFP_KERNEL);
  if (proc_read_text == NULL)
  {
    kdebug(("io_rcc(init_module): error from kmalloc\n"));
    return(-EFAULT);
  }

  io_rcc_file = create_proc_entry("io_rcc", 0644, NULL);
  if (io_rcc_file == NULL)
  {
    kdebug(("io_rcc(init_module): error from call to create_proc_entry\n"));
    return (-ENOMEM);
  }

  strcpy(io_proc_data.name, "io_rcc");
  strcpy(io_proc_data.value, "io_rcc");
  io_rcc_file->data = &io_proc_data;
  io_rcc_file->read_proc = io_rcc_read_procmem;
  io_rcc_file->write_proc = io_rcc_write_procmem;
  io_rcc_file->owner = THIS_MODULE;

  for(loop = 0; loop < IO_MAX_PCI; loop++) 
    pcidev[loop].pid = 0;
  
  kdebug(("io_rcc(init_module): driver loaded; major device number = %d\n", iorcc_major));
  return(0);
}


/***********************/
void cleanup_module(void)
/***********************/
{
  if (unregister_chrdev(iorcc_major, "io_rcc") != 0) 
    {kdebug(("io_rcc(cleanup_module): cleanup_module failed\n"));}

  remove_proc_entry("io_rcc", NULL);
  kfree(proc_read_text);

  kdebug(("io_rcc(cleanup_module): driver removed\n"));
}





