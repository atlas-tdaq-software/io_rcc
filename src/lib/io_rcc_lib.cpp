/************************************************************************/
/*									*/
/*  This is the IO_RCC library 	   				        */
/*  Its purpose is to provide user applications with access to  	*/
/*  PCI MEM an PC I/O space 						*/
/*									*/
/*   6. Jun. 02  MAJO  created						*/
/*									*/
/*******C 2002 - The software with that certain something****************/

#include <iostream>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/time.h>

//#ifdef _SLC5_ 
  //#include <asm/page.h>             //SLC5 32bit
//#endif

//#ifdef _SLC6_ 
  //#include <asm-generic/page.h>     //SLC6 64bit
//#endif

#include "rcc_error/rcc_error.h"
#include "DFDebug/DFDebug.h"
#include "io_rcc/io_rcc.h"
#include "io_rcc/io_rcc_driver.h"

//Globals
static int fd, is_open=0;

/**************************/
IO_ErrorCode_t IO_Open(void)
/**************************/
{  
  int ret;
  
  //we need to open the driver only once
  if (is_open)
  {
    is_open++;             //keep track of multiple open calls
    return(RCC_ERROR_RETURN(0, IO_RCC_SUCCESS));
  }

  //open the error package
  ret = rcc_error_init(P_ID_IO_RCC, IO_RCC_err_get);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_Open: Failed to open error package");
    return(RCC_ERROR_RETURN(0, IO_RCC_ERROR_FAIL)); 
  }
  DEBUG_TEXT(DFDB_IORCC, 15 ,"IO_Open: error package opened"); 

  DEBUG_TEXT(DFDB_IORCC, 15 ,"IO_Open: Opening /dev/io_rcc");
  if ((fd = open("/dev/io_rcc", O_RDWR)) < 0)
  {
    perror("open");
    return(RCC_ERROR_RETURN(0, IO_RCC_FILE)); 
  }
  DEBUG_TEXT(DFDB_IORCC, 15 ,"IO_Open: /dev/io_rcc is open");
  
  is_open = 1;
  
  return(IO_RCC_SUCCESS);
}


/***************************/
IO_ErrorCode_t IO_Close(void)
/***************************/
{
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));

  DEBUG_TEXT(DFDB_IORCC, 15 ,"IO_Close: called");
 
  if (is_open > 1)
    is_open--;
  else
  {
    close(fd);
    is_open = 0;
  }
  
  DEBUG_TEXT(DFDB_IORCC, 15 ,"IO_Close: done");
  return(IO_RCC_SUCCESS);
}


/*************************************************************************/
IO_ErrorCode_t IO_PCIMemMap(u_long pci_addr, u_int size, u_long *virt_addr)
/*************************************************************************/
{
  void *vaddr;
  u_long offset;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));

  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCMemMap: mapping " << size << " bytes at physical address = 0x" << HEX(pci_addr));

  offset = pci_addr & 0xfff;  //mmap seems to need a 4K alignment
  pci_addr &= 0xfffffffffffff000ll;

  vaddr = mmap(0, size, (PROT_READ|PROT_WRITE), MAP_SHARED, fd, pci_addr);
  if (vaddr == MAP_FAILED)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIMemMap: Error from mmap, errno = 0x" << HEX(errno));
    *virt_addr = 0;
    return(RCC_ERROR_RETURN(0, IO_RCC_MMAP));
  }    
  
  *virt_addr = (u_long)vaddr + offset;
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCMemMap: virtual address = 0x" << HEX(*virt_addr));
  return(IO_RCC_SUCCESS);
}


/*********************************************************/
IO_ErrorCode_t IO_PCIMemUnmap(u_long virt_addr, u_int size)
/*********************************************************/
{
  int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCIMemUnmap: Unmapping " << size << " bytes at virtual address 0x" << HEX(virt_addr));
  
  ret = munmap((void *)(virt_addr & 0xfffffffffffff000ll), size);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIMemUnmap: Error from munmap, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, IO_RCC_MUNMAP));
  }

  return(IO_RCC_SUCCESS);
}


/************************************************************/
IO_ErrorCode_t IO_PCIIoMemReadUInt(u_int address, u_int *data)
/************************************************************/
{
  //Note: this function is only provided for convenience. They can be useful for Java native interfaces
  //In a C-context the programmer should use IO_PCIMemMap directly.
  
  int ret;
  u_int *pci_ptr;
  u_long virtaddr;
    
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
  
  if (address & 0x3)
    return(RCC_ERROR_RETURN(0, IO_RCC_ILL_OFFSET));
          
  ret = IO_PCIMemMap(address, 0x10, &virtaddr);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIIoMemReadUInt: Error from IO_PCIMemMap");
    return(RCC_ERROR_RETURN(0, IO_RCC_MUNMAP));
  }
   
  pci_ptr = (u_int *)virtaddr;
  *data = *pci_ptr;
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCIIoMemReadUInt: 0x" << HEX(*data) << " read from address 0x" << HEX(address));

  ret = IO_PCIMemUnmap(virtaddr, 0x10);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIIoMemReadUInt: Error from IO_PCIMemUnmap");
    return(RCC_ERROR_RETURN(0, IO_RCC_MUNMAP));
  }

  return(IO_RCC_SUCCESS);
}


/************************************************************/
IO_ErrorCode_t IO_PCIIoMemWriteUInt(u_int address, u_int data)
/************************************************************/
{
  //Note: this function is only provided for convenience. They can be useful for Java native interfaces
  //In a C-context the programmer should use IO_PCIMemMap directly.
  
  int ret;
  u_int *pci_ptr;
  u_long virtaddr;
    
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
  
  if (address & 0x3)
    return(RCC_ERROR_RETURN(0, IO_RCC_ILL_OFFSET));
          
  ret = IO_PCIMemMap(address, 0x10, &virtaddr);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIIoMemWriteUInt: Error from IO_PCIMemMap");
    return(RCC_ERROR_RETURN(0, IO_RCC_MUNMAP));
  }
   
  pci_ptr = (u_int *)virtaddr;
  *pci_ptr = data;
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCIIoMemWriteUInt: 0x" << HEX(data) << " written to address 0x" << HEX(address));

  ret = IO_PCIMemUnmap(virtaddr, 0x10);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIIoMemWriteUInt: Error from IO_PCIMemUnmap");
    return(RCC_ERROR_RETURN(0, IO_RCC_MUNMAP));
  }

  return(IO_RCC_SUCCESS);
}


/*******************************/
IO_ErrorCode_t IO_Mark(u_int bus)
/*******************************/
{
  int ret;
  u_int addr, data, loop;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));

  if (bus < 10)
  {
    addr = 0x80000000 + (bus << 16);
    ret = IO_IOPokeUInt(0xcf8, addr);
    if (ret)
    {
      DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_Mark: Error from IO_IOPokeUInt");
      return(ret);
    }
    ret = IO_IOPeekUInt(0xcfc, &data);
    if (ret)
    {
      DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_Mark: Error from IO_IOPeekUInt");
      return(ret);
    }
  }
  else
  {
    for(loop = 0; loop < 10; loop++)
    {
      addr = 0x80000000 + (loop << 16);
      ret = IO_IOPokeUInt(0xcf8, addr);
      if (ret)
      {
	DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_Mark: Error from IO_IOPokeUInt");
	return(ret);
      }
      ret = IO_IOPeekUInt(0xcfc, &data);
      if (ret)
      {
	DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_Mark: Error from IO_IOPeekUInt");
	return(ret);
      }
    }
  }

  return(IO_RCC_SUCCESS);
}


/******************************************************/
IO_ErrorCode_t IO_IOPeekUInt(u_int address, u_int *data)
/******************************************************/
{
  int ret;
  IO_RCC_IO_t params;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
  
  if (address & 0x3)
    return(RCC_ERROR_RETURN(0, IO_RCC_ILL_OFFSET));
  
  params.offset = address;
  params.size = 4;

  ret = ioctl(fd, IOPEEK, &params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_IOPeekUInt: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  *data = params.data;
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_IOPeekUInt: 0x" << HEX(*data) << " read from address 0x" << HEX(address));
  
  return(IO_RCC_SUCCESS);
}


/**********************************************************/
IO_ErrorCode_t IO_IOPeekUShort(u_int address, u_short *data)
/**********************************************************/
{
  int ret;
  IO_RCC_IO_t params;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
  
  if (address & 0x1)
    return(RCC_ERROR_RETURN(0, IO_RCC_ILL_OFFSET));
  
  params.offset = address;
  params.size = 2;

  ret = ioctl(fd, IOPEEK, &params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_IOPeekUShort: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  *data = (u_short)params.data;
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_IOPeekUShort: 0x" << HEX((u_int)*data) << " read from address 0x" << HEX(address));
  
  return(IO_RCC_SUCCESS);
}


/********************************************************/
IO_ErrorCode_t IO_IOPeekUChar(u_int address, u_char *data)
/********************************************************/
{
  int ret;
  IO_RCC_IO_t params;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
  
  params.offset = address;
  params.size = 1;
  
  ret = ioctl(fd, IOPEEK, &params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_IOPeekUChar: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  *data = (u_char)params.data;
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_IOPeekUChar: 0x" << HEX((u_int)*data) << " read from address 0x" << HEX(address));
  
  return(IO_RCC_SUCCESS);
}


/*****************************************************/
IO_ErrorCode_t IO_IOPokeUInt(u_int address, u_int data)
/*****************************************************/
{
  int ret;
  IO_RCC_IO_t params;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));

  if (address & 0x3)
    return(RCC_ERROR_RETURN(0, IO_RCC_ILL_OFFSET));

  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_IOPokeUInt: Writing 0x" << HEX(data) << " to address 0x" << HEX(address));

  params.offset = address;
  params.data = data;
  params.size = 4;
  
  ret = ioctl(fd, IOPOKE, &params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_IOPokeUInt: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(IO_RCC_SUCCESS);
}


/*********************************************************/
IO_ErrorCode_t IO_IOPokeUShort(u_int address, u_short data)
/*********************************************************/
{
  int ret;
  IO_RCC_IO_t params;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));

  if (address & 0x1)
    return(RCC_ERROR_RETURN(0, IO_RCC_ILL_OFFSET));
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_IOPokeUShort: Writing 0x" << HEX((u_int)data) << " to address 0x" << HEX(address));

  params.offset = address;
  params.data = data;
  params.size = 2;

  ret = ioctl(fd, IOPOKE, &params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_IOPokeUShort: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(IO_RCC_SUCCESS);
}


/*******************************************************/
IO_ErrorCode_t IO_IOPokeUChar(u_int address, u_char data)
/*******************************************************/
{
  int ret;
  IO_RCC_IO_t params;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_IOPokeUChar: Writing 0x" << HEX((u_int)data) << " to address 0x" << HEX(address));

  params.offset = address;
  params.data = data;
  params.size = 1;
  
  ret = ioctl(fd, IOPOKE, &params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_IOPokeUChar: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(IO_RCC_SUCCESS);
}


/************************************************************************************************/
IO_ErrorCode_t IO_PCIDeviceLink(u_int vendor_id, u_int device_id, u_int occurrence, u_int *handle)
/************************************************************************************************/
{
  IO_PCI_FIND_t params;
  int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
 
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCIDeviceLink: vendor_id  = 0x" << HEX(vendor_id));
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCIDeviceLink: device_id  = 0x" << HEX(device_id));
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCIDeviceLink: occurrence = " << occurrence);
  
  params.vid = vendor_id;
  params.did = device_id;
  params.occ = occurrence;

  if (occurrence == 0)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIDeviceLink: occurrence must not be 0");
    return(RCC_ERROR_RETURN(0, IO_PCI_NOT_FOUND));
  }
  
  ret = ioctl(fd, IOPCILINK, &params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIDeviceLink: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  *handle = params.handle;
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCIDeviceLink: handle = " << *handle);
  return(IO_RCC_SUCCESS);
}


/*************************************************************/
IO_ErrorCode_t IO_PCIDeviceInfo(u_int handle, pci_info_t *info)
/*************************************************************/
{
  int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));  
    
  info->handle = handle;
  
  ret = ioctl(fd, IOPCIINFO, info);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIDeviceInfo: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(IO_RCC_SUCCESS);
}


/*********************************************/
IO_ErrorCode_t IO_PCIDeviceUnlink(u_int handle)
/*********************************************/
{
  int ret;
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCIDeviceUnlink: Unlinking handle" << handle);
  
  ret = ioctl(fd, IOPCIUNLINK, &handle);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIDeviceUnlink: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }     
        
  return(IO_RCC_SUCCESS);
}


/**************************************************************************/
IO_ErrorCode_t IO_PCIConfigReadUInt(u_int handle, u_int offset, u_int *data)
/**************************************************************************/
{
  int ret;
  IO_PCI_CONF_t params;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
  
  if (offset & 0x3)
    return(RCC_ERROR_RETURN(0, IO_RCC_ILL_OFFSET));
    
  params.handle = handle;
  params.offs = offset;
  params.size = 4;
  
  ret = ioctl(fd, IOPCICONFR, &params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIConfigReadUInt: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }   
  
  *data = params.data;
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCIConfigReadUInt: 0x" << HEX(*data) << " read from offset 0x" << HEX(offset));
  return(IO_RCC_SUCCESS);
}


/******************************************************************************/
IO_ErrorCode_t IO_PCIConfigReadUShort(u_int handle, u_int offset, u_short *data)
/******************************************************************************/
{
  int ret;
  IO_PCI_CONF_t params;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
  
  if (offset & 0x1)
    return(RCC_ERROR_RETURN(0, IO_RCC_ILL_OFFSET));
  
  params.handle = handle;
  params.offs = offset;
  params.size = 2;

  ret = ioctl(fd, IOPCICONFR, &params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIConfigReadUShort: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }   
  
  *data = (u_short)params.data;
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCIConfigReadUShort: 0x" << HEX((u_int)*data) << " read from offset 0x" << HEX(offset));
  return(IO_RCC_SUCCESS);
}


/****************************************************************************/
IO_ErrorCode_t IO_PCIConfigReadUChar(u_int handle, u_int offset, u_char *data)
/****************************************************************************/
{
  int ret;
  IO_PCI_CONF_t params;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
  
  params.handle = handle;
  params.offs = offset;
  params.size = 1;

  ret = ioctl(fd, IOPCICONFR, &params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIConfigReadUChar: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }   
  
  *data = (u_char)params.data;
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCIConfigReadUChar: 0x" << HEX((u_int)*data) << " read from offset 0x" << HEX(offset));
  return(IO_RCC_SUCCESS);
}


/**************************************************************************/
IO_ErrorCode_t IO_PCIConfigWriteUInt(u_int handle, u_int offset, u_int data)
/**************************************************************************/
{
  int ret;
  IO_PCI_CONF_t params;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
    
  if (offset & 0x3)
    return(RCC_ERROR_RETURN(0, IO_RCC_ILL_OFFSET));
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCIConfigWriteUInt: Writing 0x" << HEX((u_int)data) << " to offset 0x" << HEX(offset));
    
  params.handle = handle;
  params.offs = offset;
  params.data = data;  
  params.size = 4;

  ret = ioctl(fd, IOPCICONFW, &params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIConfigWriteUInt: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }   

  return(IO_RCC_SUCCESS);
}


/******************************************************************************/
IO_ErrorCode_t IO_PCIConfigWriteUShort(u_int handle, u_int offset, u_short data)
/******************************************************************************/
{
  int ret;
  IO_PCI_CONF_t params;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
    
  if (offset & 0x1)
    return(RCC_ERROR_RETURN(0, IO_RCC_ILL_OFFSET));
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCIConfigWriteUShort: Writing 0x" << HEX((u_int)data) << " to offset 0x" << HEX(offset));
    
  params.handle = handle;
  params.offs = offset;
  params.data = data;  
  params.size = 2;

  ret = ioctl(fd, IOPCICONFW, &params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIConfigWriteUShort: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }   

  return(IO_RCC_SUCCESS);
}


/****************************************************************************/
IO_ErrorCode_t IO_PCIConfigWriteUChar(u_int handle, u_int offset, u_char data)
/****************************************************************************/
{
  int ret;
  IO_PCI_CONF_t params;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
  DEBUG_TEXT(DFDB_IORCC, 20 ,"IO_PCIConfigWriteUChar: Writing 0x" << HEX((u_int)data) << " to offset 0x" << HEX(offset));
  
  params.handle = handle;
  params.offs = offset;
  params.data = data;  
  params.size = 1;

  ret = ioctl(fd, IOPCICONFW, &params);
  if (ret)
  {
    DEBUG_TEXT(DFDB_IORCC, 5 ,"IO_PCIConfigWriteUChar: Error from ioctl, errno = 0x" << HEX(errno));
    return(RCC_ERROR_RETURN(0, errno));
  }   

  return(IO_RCC_SUCCESS);
}


/**************************************************/
IO_ErrorCode_t IO_GetHostInfo(HostInfo_t *host_info)
/**************************************************/
{
  int ret;
  u_char d1;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, IO_RCC_NOTOPEN));
    
  // Read the board identification
  ret = IO_IOPokeUChar(CMOSA, BID1);
  if (ret != IO_RCC_SUCCESS)
    return(RCC_ERROR_RETURN(0, IO_RCC_IOFAIL));
 
  ret = IO_IOPeekUChar(CMOSD, &d1);
  if (ret != IO_RCC_SUCCESS)
    return(RCC_ERROR_RETURN(0, IO_RCC_IOFAIL));

  if (!(d1 & 0x80))
    return(RCC_ERROR_RETURN(0, IO_RCC_ILLMANUF));
  
  ret = IO_IOPokeUChar(CMOSA, BID2);
  if (ret != IO_RCC_SUCCESS)
    return(RCC_ERROR_RETURN(0, IO_RCC_IOFAIL));

  ret = IO_IOPeekUChar(CMOSD, &d1);
  if (ret != IO_RCC_SUCCESS)
    return(RCC_ERROR_RETURN(0, IO_RCC_IOFAIL));


  d1 &= 0x1f;  // Mask board ID bits
  host_info->board_type = VP_UNKNOWN;
       if (d1 == 2) host_info->board_type = VP_PSE;
  else if (d1 == 3) host_info->board_type = VP_PSE;
  else if (d1 == 4) host_info->board_type = VP_PSE;
  else if (d1 == 5) host_info->board_type = VP_PMC;
  else if (d1 == 6) host_info->board_type = VP_CP1;
  else if (d1 == 7) host_info->board_type = VP_100;
  
  host_info->board_manufacturer = CCT;
  host_info->operating_system_type = LINUX;
  host_info->operating_system_version = K249;  

  return(IO_RCC_SUCCESS);
}


/***********************************************************/
u_int IO_RCC_err_get(err_pack err, err_str pid, err_str code)
/***********************************************************/
{ 
  strcpy(pid, P_ID_IO_RCC_STR);

  switch (RCC_ERROR_MINOR(err))
  {  
    case IO_RCC_SUCCESS:       strcpy(code, IO_RCC_SUCCESS_STR);         break;
    case IO_RCC_FILE:          strcpy(code, IO_RCC_FILE_STR);            break;
    case IO_RCC_NOTOPEN:       strcpy(code, IO_RCC_NOTOPEN_STR);         break;
    case IO_RCC_MMAP:          strcpy(code, IO_RCC_MMAP_STR);            break;
    case IO_RCC_MUNMAP :       strcpy(code, IO_RCC_MUNMAP_STR);          break;
    case IO_RCC_ILLMANUF:      strcpy(code, IO_RCC_ILLMANUF_STR);        break;
    case IO_RCC_IOFAIL:        strcpy(code, IO_RCC_IOFAIL_STR);          break;
    case IO_PCI_TABLEFULL:     strcpy(code, IO_PCI_TABLEFULL_STR);       break;
    case IO_PCI_NOT_FOUND:     strcpy(code, IO_PCI_NOT_FOUND_STR);       break;
    case IO_PCI_ILL_HANDLE:    strcpy(code, IO_PCI_ILL_HANDLE_STR);      break;
    case IO_PCI_UNKNOWN_BOARD: strcpy(code, IO_PCI_UNKNOWN_BOARD_STR);   break;
    case IO_PCI_CONFIGRW:      strcpy(code, IO_PCI_CONFIGRW_STR);        break;
    case IO_PCI_REMAP:         strcpy(code, IO_PCI_REMAP_STR);           break;
    case IO_RCC_ILL_OFFSET:    strcpy(code, IO_RCC_ILL_OFFSET_STR);      break;
    default:                   strcpy(code, IO_RCC_NO_CODE_STR); return(RCC_ERROR_RETURN(0,IO_RCC_NO_CODE)); break;
  }
  return(RCC_ERROR_RETURN(0, IO_RCC_SUCCESS));
}


