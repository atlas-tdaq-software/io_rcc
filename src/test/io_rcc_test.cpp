/************************************************************************/
/*									*/
/* File: io_rcc_test.c							*/
/*									*/
/* This is the test program for the IO_RCC library			*/
/*									*/
/*  7. Jun. 02  MAJO  created						*/
/*									*/
/**************** C 2002 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "io_rcc/io_rcc.h"

//globals

//Prototypes
int mainhelp(void);
int func_menu(void);
int setdebug(void);
int cmos_menu(void);
int read_cmos(void);
int write_cmos(void);

/************/
int main(void)
/************/
{
  int fun=1;

  printf("\n\n\nThis is the test program for the IO RCC library \n");
  printf("====================================================\n");

  while (fun != 0)  
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 Help                        2 Test functions\n");
    printf("   3 Set debug parameters        4 CMOS menu\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) func_menu();
    if (fun == 3) setdebug();
    if (fun == 4) cmos_menu();
  }
  return(0);  
}


/****************/
int setdebug(void)
/****************/
{
  u_int dblevel = 0, dbpackage = DFDB_IORCC;

  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("\n=========================================================================\n");
  printf("Contact markus.joos@cern.ch if you need help\n");
  printf("=========================================================================\n\n");
  return(0);
}


/*****************/
int func_menu(void)
/*****************/
{
  int fun = 1;
  IO_ErrorCode_t ret;
  static u_long ldata, virtaddr = 0, pciaddr = 0x80000000;
  static u_int ioreg = 0x210, pcisize = 0x1000;
  static u_int idata = 0, vid = 0x10e3, did = 0, occ = 1, handle = 0;
  static u_int offs = 0, busmark = 1;
  static u_short sdata = 0;
  static u_char cdata = 0x0;
  u_int *ptr32, loop;
  u_char *ptr8;
  u_short *ptr16;
  u_long *ptr64;
  HostInfo_t info;
  pci_info_t pciinfo;
    
  printf("\n=========================================================================\n");
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a function of the API:\n");
    printf("   1 IO_Open                     2 IO_Close\n");
    printf("   3 IO_IOPeekUInt               4 IO_IOPokeUInt\n");
    printf("   5 IO_IOPeekUShort             6 IO_IOPokeUShort\n");
    printf("   7 IO_IOPeekUChar              8 IO_IOPokeUChar\n");
    printf("   9 IO_PCIDeviceLink           10 IO_PCIDeviceUnlink\n");
    printf("  11 IO_PCIConfigReadUInt       12 IO_PCIConfigWriteUInt\n");
    printf("  13 IO_PCIConfigReadUShort     14 IO_PCIConfigWriteUShort\n"); 
    printf("  15 IO_PCIConfigReadUChar      16 IO_PCIConfigWriteUChar\n");
    printf("  17 IO_PCIMemMap               18 IO_PCIMemUnmap\n");
    printf("  19 IO_GetHostInfo             20 IO_PCIDeviceInfo\n");
    printf("  21 IO_Mark\n");
    printf("  22 IO_PCIIoMemReadUInt        23  IO_PCIIoMemWriteUInt\n");
    printf("==========================================================\n");
    printf("  31  8-bit Read                32  8-bit Write\n");
    printf("  33 16-bit Read                34 16-bit Write\n");
    printf("  35 32-bit Read                36 32-bit Write\n");
    printf("  37 64-bit Read                38 64-bit Write\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun); 
    
    if(fun == 1)
    {
      ret = IO_Open();
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("IO_RCC library opened\n");
    }
    
    if (fun == 2) 
    {
      ret = IO_Close();
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("IO_RCC library closed\n");
    }    

    if (fun == 3) 
    {
      printf("Enter the address of the I/O register ");
      ioreg = gethexd(ioreg);
      ret = IO_IOPeekUInt(ioreg, &idata);
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The data read is 0x%08x\n", idata);
    }    

    if (fun == 4) 
    {     
      printf("Enter the address of the I/O register ");
      ioreg = gethexd(ioreg);
      printf("Enter the data to be written ");
      idata = gethexd(idata);
      ret = IO_IOPokeUInt(ioreg, idata);
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The data has been written\n");
    }    

    if (fun == 5) 
    {
      printf("Enter the address of the I/O register ");
      ioreg = gethexd(ioreg);
      ret = IO_IOPeekUShort(ioreg, &sdata);
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The data read is 0x%04x\n", sdata);
    }    

    if (fun == 6) 
    {     
      printf("Enter the address of the I/O register ");
      ioreg = gethexd(ioreg);
      printf("Enter the data to be written ");
      sdata = gethexd(sdata);
      ret = IO_IOPokeUShort(ioreg, sdata);
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The data has been written\n");
    }
    
    if (fun == 7) 
    {
      printf("Enter the address of the I/O register ");
      ioreg = gethexd(ioreg);
      ret = IO_IOPeekUChar(ioreg, &cdata);
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The data read is 0x%02x\n", cdata);
    }    

    if (fun == 8) 
    {     
      printf("Enter the address of the I/O register ");
      ioreg = gethexd(ioreg);
      printf("Enter the data to be written ");
      cdata = gethexd(cdata);
      ret = IO_IOPokeUChar(ioreg, cdata);
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The data has been written\n");
    }

    if (fun == 9) 
    {
      printf("Enter the Vendor ID ");
      vid = gethexd(vid);
      printf("Enter the Device ID ");
      did = gethexd(did);
      printf("Enter the occurrence ");
      occ = getdecd((int)occ);
      ret = IO_PCIDeviceLink(vid, did, occ, &handle);
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("Device found. Handle = %d\n", handle);
    }    

    if (fun == 10) 
    {
      printf("Enter the handle ");
      handle = getdecd((int)handle);
      ret = IO_PCIDeviceUnlink(handle);
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The device has been unulinked\n");
    }    

    if (fun == 11) 
    {
      printf("Enter the handle of the device ");
      handle = getdecd((int)handle);
      printf("Enter the offset of the configuration register ");
      offs = gethexd(offs);
      ret = IO_PCIConfigReadUInt(handle, offs, &idata);      
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The data read from the register is 0x%08x\n", idata);
    }    

    if (fun == 12) 
    {    
      printf("Enter the handle of the device ");
      handle = getdecd((int)handle);
      printf("Enter the offset of the configuration register ");
      offs = gethexd(offs);
      printf("Enter the data to write ");
      idata = gethexd(idata);
      ret = IO_PCIConfigWriteUInt(handle, offs, idata);      
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The data has been written\n");
        
    } 
    
    if (fun == 13) 
    {
      printf("Enter the handle of the device ");
      handle = getdecd((int)handle);
      printf("Enter the offset of the configuration register ");
      offs = gethexd(offs);
      ret = IO_PCIConfigReadUShort(handle, offs, &sdata);      
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The data read from the register is 0x%04x\n", sdata);
    }    

    if (fun == 14) 
    {    
      printf("Enter the handle of the device ");
      handle = getdecd((int)handle);
      printf("Enter the offset of the configuration register ");
      offs = gethexd(offs);
      printf("Enter the data to write ");
      sdata = gethexd(sdata);
      ret = IO_PCIConfigWriteUShort(handle, offs, sdata);      
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The data has been written\n");
        
    } 
    
    if (fun == 15) 
    {
      printf("Enter the handle of the device ");
      handle = getdecd((int)handle);
      printf("Enter the offset of the configuration register ");
      offs = gethexd(offs);
      ret = IO_PCIConfigReadUChar(handle, offs, &cdata);      
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The data read from the register is 0x%02x\n", cdata);
    }    

    if (fun == 16) 
    {    
      printf("Enter the handle of the device ");
      handle = getdecd((int)handle);
      printf("Enter the offset of the configuration register ");
      offs = gethexd(offs);
      printf("Enter the data to write ");
      cdata = gethexd(cdata);
      ret = IO_PCIConfigWriteUChar(handle, offs, cdata);      
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The data has been written\n");
        
    }  
     
    if (fun == 17) 
    {
      printf("Enter the PCI address ");
      pciaddr = gethexd(pciaddr);
      printf("Enter the mapping size ");
      pcisize = gethexd(pcisize);
      
      ret = IO_PCIMemMap(pciaddr, pcisize, &virtaddr);
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The virtual address is 0x%016lx\n", (u_long)virtaddr);
    }    

    if (fun == 18) 
    {   
      printf("Enter the virtual address ");
      virtaddr = gethexd(virtaddr);
      printf("Enter the mapping size ");
      pcisize = gethexd(pcisize);
      ret = IO_PCIMemUnmap(virtaddr, pcisize);    
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
        printf("The mapping has been closed\n");
    }    
    
    if (fun == 19) 
    {
      ret = IO_GetHostInfo(&info);
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
      {
        printf("Board manufacturer:      %d\n",info.board_manufacturer);       
        printf("Board type:              %d\n",info.board_type);
        printf("Operating system type:   %d\n",info.operating_system_type);
        printf("Operating systemversion: %d\n",info.operating_system_version);
      }
    }

    if (fun == 20) 
    {
      printf("Enter the handle of the device ");
      handle = getdecd((int)handle);
      ret = IO_PCIDeviceInfo(handle, &pciinfo);
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret);
      else
      {
        for(loop = 0; loop < 6; loop++)
        {
          printf("BAR %d: base = 0x%08x   size = 0x%08x   flags = ", loop, pciinfo.bar[loop].base, pciinfo.bar[loop].size);
          if (pciinfo.bar[loop].flags == IORESOURCE_IO)
            printf("IO space\n");
          else if (pciinfo.bar[loop].flags == IORESOURCE_MEM)
            printf("MEM space\n");
          else
            printf("Undefined\n");
        }
      }
    }
    
    if (fun == 21)
    {
      printf("The IO_Mark function generates a PCI config cycles to the base address of device 0 on\n");
      printf("the selected PCI bus. Enter a number between 0 and 9 to address a known bus\n");
      printf("or 10 to create such a marker cycle on all busses from 0 to 9\n\n");
      printf("Enter bus number: ");
      busmark = getdecd(busmark);
      ret = IO_Mark(busmark);
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret); 
    } 	
	
	
    if (fun == 22)
    {
      ret = 0;
      static u_int pci_addr = 0, pci_data = 0;
      
      printf("Enter the address ");
      pci_addr = gethexd(pci_addr);

      IO_PCIIoMemReadUInt(pci_addr, &pci_data);
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret); 
	
      printf("Data read = 0x%08x\n", pci_data);    
    }
    
     
    if (fun == 23)
    {   	
      static u_int pci_addr = 0, pci_data = 0;
      ret = 0;
      
      printf("Enter the address ");
      pci_addr = gethexd(pci_addr);
      
      printf("Enter the data ");
      pci_data = gethexd(pci_data); 
           
      IO_PCIIoMemWriteUInt(pci_addr, pci_data);
      if (ret != IO_RCC_SUCCESS)
	rcc_error_print(stdout, ret); 
	
      printf("Data written\n");
    }

	
    if (fun == 31)
    {
      printf("Enter the virtual address: ");
      virtaddr = gethexd(virtaddr);
      ptr8 = (u_char *)virtaddr;  
      printf("Data at 0x%16lx = 0x%02x\n",(u_long)ptr8, *ptr8);
    }
    
    if (fun == 32)
    {
      printf("Enter the virtual address: ");
      virtaddr = gethexd(virtaddr);
      printf("Enter the 8-bit data word to write: ");
      cdata = gethexd(cdata);
      ptr8 = (u_char *)virtaddr;
      *ptr8 = cdata;
      printf("Data written\n");
    }

    if (fun == 33)
    {
      printf("Enter the virtual address: ");
      virtaddr = gethexd(virtaddr);
      ptr16 = (u_short *)virtaddr;  
      printf("Data at 0x%16lx = 0x%04x\n",(u_long)ptr16, *ptr16);
    }
    
    if (fun == 34)
    {
      printf("Enter the virtual address: ");
      virtaddr = gethexd(virtaddr);
      printf("Enter the 16-bit data word to write: ");
      sdata = gethexd(sdata);
      ptr16 = (u_short *)virtaddr;
      *ptr16 = sdata;
      printf("Data written\n");
    }
    
    if (fun == 35)
    {
      printf("Enter the virtual address: ");
      virtaddr = gethexd(virtaddr);
      ptr32 = (u_int *)virtaddr;  
      printf("Data at 0x%16lx = 0x%08x\n",(u_long)ptr32, *ptr32);
    }
    
    if (fun == 36)
    {
      printf("Enter the virtual address: ");
      virtaddr = gethexd(virtaddr);
      printf("Enter the 32-bit data word to write: ");
      idata = gethexd(idata);
      ptr32 = (u_int *)virtaddr;
      *ptr32 = idata;
      printf("Data written\n");
    }
    
    if (fun == 37)
    {
      printf("Enter the virtual address: ");
      virtaddr = gethexd(virtaddr);
      ptr64 = (u_long *)virtaddr;  
      printf("Data at 0x%16lx = 0x%16lx\n",(u_long)ptr64, *ptr64);
    }
    
    if (fun == 38)
    {
      printf("Enter the virtual address: ");
      virtaddr = gethexd(virtaddr);
      printf("Enter the 64-bit data word to write: ");
      ldata = gethexd(ldata);
      ptr64 = (u_long *)virtaddr;
      *ptr64 = ldata;
      printf("Data written\n");
    }
  }
  printf("=========================================================================\n\n");
  return(0);
}


/*****************/
int cmos_menu(void)
/*****************/
{
  int fun = 1;

  printf("\n=========================================================================\n");
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a function of the API:\n");
    printf("   1 Read CMOS\n");
    printf("   2 Write CMOS\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun); 
    
    if(fun == 1)
      read_cmos();
          
    if (fun == 2) 
      write_cmos();
  }
  printf("=========================================================================\n\n");
  return(0);
}


/*****************/
int read_cmos(void)
/*****************/
{
  IO_ErrorCode_t ret;
  u_int loop;
  u_char cdata;

  ret = IO_Open();
  if (ret != IO_RCC_SUCCESS)
    rcc_error_print(stdout, ret);

  for (loop = 0; loop < 128; loop++)
  {
    ret = IO_IOPokeUChar(0x70, loop);
    if (ret != IO_RCC_SUCCESS)
      rcc_error_print(stdout, ret);

    ret = IO_IOPeekUChar(0x71, &cdata);
    if (ret != IO_RCC_SUCCESS)
      rcc_error_print(stdout, ret);
    else
      printf("Offset 0x%02x: 0x%02x\n", loop, cdata);
  }
    
  ret = IO_Close();
  if (ret != IO_RCC_SUCCESS)
    rcc_error_print(stdout, ret);

  return(0);
}


/******************/
int write_cmos(void)
/******************/
{
  IO_ErrorCode_t ret;
  u_int loop;
  u_char cdata;
  u_char cmos_data[5][2] = {{0x13, 0xd2},{0x1b, 0x6b},{0x2f, 0x25},{0x4f, 0xa0},{0x50, 0x17}};
  #define NUMREF 120
  u_char ref_data[NUMREF][2] = {
                 {0x01, 0x00},                 {0x03, 0x00},                 {0x05, 0x00},    
                 {0x0a, 0x26},  {0x0b, 0x02},                 {0x0d, 0x80},  {0x0e, 0x00},  {0x0f, 0x00},
  {0x10, 0x00},  {0x11, 0x90},  {0x12, 0x00},  {0x13, 0xc2},  {0x14, 0x03},  {0x15, 0x70},  {0x16, 0x02},  {0x17, 0x00},  
  {0x18, 0xfc},  {0x19, 0x00},  {0x1a, 0x00},  {0x1b, 0x6a},  {0x1c, 0x14},  {0x1d, 0x81},  {0x1e, 0x68},  {0x1f, 0xac},  
  {0x20, 0x8f},  {0x21, 0x68},  {0x22, 0xac},  {0x23, 0x0f},  {0x24, 0x0c},  {0x25, 0x00},  {0x26, 0x00},  {0x27, 0x00},
  {0x28, 0x00},  {0x29, 0x00},  {0x2a, 0x00},  {0x2b, 0x00},  {0x2c, 0x00},  {0x2d, 0x80},  {0x2e, 0x07},  {0x2f, 0x14},  
  {0x30, 0x00},  {0x31, 0xfc},  {0x32, 0x20},  {0x33, 0xc8},  {0x34, 0xcf},  {0x35, 0xa0},  {0x36, 0x60},  {0x37, 0x00},  
  {0x38, 0x00},  {0x39, 0x01},  {0x3a, 0x00},  {0x3b, 0x00},  {0x3c, 0x00},  {0x3d, 0x00},  {0x3e, 0x08},  {0x3f, 0xf8},
  {0x40, 0x1b},  {0x41, 0x00},  {0x42, 0x00},  {0x43, 0x00},  {0x44, 0x00},  {0x45, 0x80},  {0x46, 0x00},  {0x47, 0x00},  
  {0x48, 0x00},  {0x49, 0x00},  {0x4a, 0x00},  {0x4b, 0x00},  {0x4c, 0x00},  {0x4d, 0x06},  {0x4e, 0x00},  {0x4f, 0xc0},  
  {0x50, 0xbf},  {0x51, 0x06},  {0x52, 0x80},  {0x53, 0x51},  {0x54, 0x23},  {0x55, 0x07},  {0x56, 0x00},  {0x57, 0x00},
  {0x58, 0x00},  {0x59, 0x65},  {0x5a, 0xac},  {0x5b, 0x7f},  {0x5c, 0x00},  {0x5d, 0x80},  {0x5e, 0x07},  {0x5f, 0xfc},  
  {0x60, 0x09},  {0x61, 0x00},  {0x62, 0x00},  {0x63, 0x00},  {0x64, 0x00},  {0x65, 0x00},  {0x66, 0x00},  {0x67, 0x80},  
  {0x68, 0x52},  {0x69, 0xc2},  {0x6a, 0xfb},  {0x6b, 0xff},  {0x6c, 0x17},  {0x6d, 0x2d},  {0x6e, 0x90},  {0x6f, 0x53},
  {0x70, 0x2c},  {0x71, 0xc0},  {0x72, 0x60},  {0x73, 0x00},  {0x74, 0x0c},  {0x75, 0x00},  {0x76, 0x00},  {0x77, 0x31},  
  {0x78, 0x81},  {0x79, 0x79},  {0x7a, 0x04},  {0x7b, 0x00},  {0x7c, 0x00},  {0x7d, 0x00},  {0x7e, 0x00},  {0x7f, 0x00}
  };

  ret = IO_Open();
  if (ret != IO_RCC_SUCCESS)
    rcc_error_print(stdout, ret);

  //Check the CMOS
  for (loop = 0; loop < NUMREF; loop++)
  {
    ret = IO_IOPokeUChar(0x70, ref_data[loop][0]);
    if (ret != IO_RCC_SUCCESS)
      rcc_error_print(stdout, ret);

    ret = IO_IOPeekUChar(0x71, &cdata);
    if (ret != IO_RCC_SUCCESS)
      rcc_error_print(stdout, ret);
    if (ref_data[loop][1] != cdata)
    {
      printf("Byte at offset 0x%02x is 0x%02x instead of 0x%02x\n", ref_data[loop][0], cdata, ref_data[loop][1]);
      printf("I will not touch the CMOS\n");
      return(0);
    }
  }
  
  printf("The CMOS is in the default state. I will now enable hyper threading/n"); 

  //Program the CMOS
  for (loop = 0; loop < 5; loop++)
  {

    printf("Writing 0x%02x at offset 0x%02x\n", cmos_data[loop][1], cmos_data[loop][0]);    
    ret = IO_IOPokeUChar(0x70, cmos_data[loop][0]);
    if (ret != IO_RCC_SUCCESS)
      rcc_error_print(stdout, ret);

    ret = IO_IOPokeUChar(0x71, cmos_data[loop][1]);
    if (ret != IO_RCC_SUCCESS)
      rcc_error_print(stdout, ret);
  }
    
  ret = IO_Close();
  if (ret != IO_RCC_SUCCESS)
    rcc_error_print(stdout, ret);

  return(0);
}







