/************************************************************************/
/*									*/
/* File: pc_speed.c							*/
/*									*/
/* This program determines that actual PCI frequencies			*/
/* of the slots of a ROS PCs						*/
/*									*/
/*  20. Oct. 09  MAJO  created						*/
/*									*/
/**************** C 2009 - A nickel program worth a dime ****************/

#include <cstdlib>
#include <stdio.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "io_rcc/io_rcc.h"

/************/
int main(void)
/************/
{
  int pci_freq;
  IO_ErrorCode_t ret;
  static u_int handle = 0;
  static u_short sdata = 0;
    
  ret = IO_Open();
  if (ret != IO_RCC_SUCCESS)
    rcc_error_print(stdout, ret);

  ret = IO_PCIDeviceLink(0x8086, 0x329, 1, &handle);
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  } 
    
  ret = IO_PCIConfigReadUShort(handle, 0x40, &sdata);      
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    exit(-2);
  }
  pci_freq = (sdata >> 9) & 0x3;
  if (pci_freq == 0)
    printf("S05R and S06R:  33 MHz\n");
  if (pci_freq == 1)
    printf("S05R and S06R:  66 MHz\n");
  if (pci_freq == 2)
    printf("S05R and S06R: 100 MHz\n");
  if (pci_freq == 3)
    printf("S05R and S06R: 133 MHz\n");

  ret = IO_PCIDeviceLink(0x8086, 0x329, 2, &handle);
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  } 
    
  ret = IO_PCIConfigReadUShort(handle, 0x40, &sdata);      
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    exit(-2);
  }
  pci_freq = (sdata >> 9) & 0x3;
  if (pci_freq == 0)
    printf("S08R and S09R:  33 MHz\n");
  if (pci_freq == 1)
    printf("S08R and S09R:  66 MHz\n");
  if (pci_freq == 2)
    printf("S08R and S09R: 100 MHz\n");
  if (pci_freq == 3)
    printf("S08R and S09R: 133 MHz\n");

  ret = IO_PCIDeviceLink(0x8086, 0x32a, 1, &handle);
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  } 
    
  ret = IO_PCIConfigReadUShort(handle, 0x40, &sdata);      
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    exit(-2);
  }
  pci_freq = (sdata >> 9) & 0x3;
  if (pci_freq == 0)
    printf("S04R:  33 MHz\n");
  if (pci_freq == 1)
    printf("S04R:  66 MHz\n");
  if (pci_freq == 2)
    printf("S04R: 100 MHz\n");
  if (pci_freq == 3)
    printf("S04R: 133 MHz\n");

  ret = IO_PCIDeviceLink(0x8086, 0x32a, 2, &handle);
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  } 
    
  ret = IO_PCIConfigReadUShort(handle, 0x40, &sdata);      
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    exit(-2);
  }
  pci_freq = (sdata >> 9) & 0x3;
  if (pci_freq == 0)
    printf("S07R:  33 MHz\n");
  if (pci_freq == 1)
    printf("S07R:  66 MHz\n");
  if (pci_freq == 2)
    printf("S07R: 100 MHz\n");
  if (pci_freq == 3)
    printf("S07R: 133 MHz\n");

  ret = IO_Close();
  if (ret != IO_RCC_SUCCESS)
    rcc_error_print(stdout, ret);
}    


